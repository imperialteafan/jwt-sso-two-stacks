const createResponse = function (statusCode, body) {
    return {
        statusCode: statusCode,
        body: typeof body === 'string'
            ? body
            : JSON.stringify(body)
    }
};

const responseSuccess = (data) => {
    return createResponse(200, data);
}

const responseError = (data) => {
    if (data.statusCode === 500) {
        return responseInternalError(data)
    }
    if (data.statusCode === 404) {
        return responseNotFound(data)
    }
    if (data.statusCode === 400) {
        return responseBadRequest(data)
    }
}

const responseBadRequest = (data) => {
    return createResponse(400, 'Bad Request: ' + data.message);
}

const responseNotFound = (data) => {
    return createResponse(404, data.message);
}

const responseInternalError = (data) => {
    return createResponse(500, 'Internal Server Error: ' + data.message);
}

module.exports = {
    responseSuccess,
    responseError
}
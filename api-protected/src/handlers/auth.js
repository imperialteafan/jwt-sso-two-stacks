const jwt = require('jsonwebtoken');

/**
 * Verifies if the User should be allowed to access the called Resource.
 * Decodes JWT token and checks User's scopes and returns a Policy.
 * @param {*} event - contains token, and methodArn which are used to determine the access
 * @param {*} context 
 * @param {*} callback - returns IAM policy
 * @returns IAM Policy either allowing or denying lambda's invocation.
 */
async function verifyAccess(event, context, callback) {
  try {
    console.log('[auth] #verifyToken called - event: ', event);
    // Read method's ARN from the event.
    const methodArn = event.methodArn;
    // Token begins with 'Bearer ' - omit that part.
    const token = event.authorizationToken.slice(7);
    // Get User's data from the token.
    const userData = jwt.verify(token, process.env.JWT_SECRET);
    // Authorize User.
    const isAllowed = authorizeUser(userData.scopes, methodArn);
    const effect = isAllowed
      ? 'Allow'
      : 'Deny'
    // Create Policy.
    const policy = generatePolicy(userData.userId, effect, methodArn);
    // Return the Policy.
    return callback(null, policy);
  } catch (error) {
    console.error(error);
    // Return 'Unauthorized' in case the token is undefined or malformed.
    return callback('Unauthorized')
  }

}

/**
 * Authorizes User based on their list of Scopes and the called Resource.
 * @param {*} userScopes - list of strings containing Resources that the user is allowed to access
 * @param {*} methodArn - ARN of the Resource protected by the API Gateway Authorizer.
 * @returns true or false
 */
function authorizeUser(userScopes, methodArn) {
  console.log(`[auth] #authorizeUser - checking ${userScopes} for method ${methodArn}`);

  let isResourceAllowed = false;
  // Check if userScopes list contains the called Resource.
  userScopes.map(scope => {
    if (methodArn.endsWith(scope)) {
      isResourceAllowed = true;
      console.log('[auth] #authorizeUser - user authorized successfully.');
      return;
    }
  });

  return isResourceAllowed;
}

/**
 * Create Policy for the User and the Resource that is protected by the API Gateway Authorizer.
 * @param {*} userId - User's id
 * @param {*} effect - 'Allow' or 'Deny'
 * @param {*} methodArn ARN of the Resource protected by the API Gateway Authorizer.
 * @returns 
 */
function generatePolicy(userId, effect, methodArn) {
  const policy = {
    principalId: userId,
    policyDocument: {
      Version: '2012-10-17',
      Statement: [
        {
          Action: 'execute-api:Invoke',
          Effect: effect,
          Resource: methodArn,
        },
      ],
    }
  }

  return policy;
}

module.exports = {
  verifyAccess
}
const apiResult = require('../../../shared/utils/apiResult');

/**
 * Simple endpoint with access regulated by API Gateway Authorizer.
 * @param {*} event 
 * @param {*} context 
 * @returns 
 */
async function hi(event, context) {
    try {
        console.log('[hi] hi called with event: ', event);

        const response = "Hi there! Authenticated using JWT and authorized based on the User's scopes.";

        console.log('[hi] returned response: ', response);
        return apiResult.responseSuccess(response);

    } catch (error) {
        console.error('[hi] hi ' + error.stack)
        return apiResult.responseError(error);
    }
}

module.exports = {
    hi
}
const apiResult = require('../../../shared/utils/apiResult');

/**
 * Simple endpoint with access regulated by API Gateway Authorizer.
 * 
 * @param {*} event 
 * @param {*} context 
 * @returns 
 */
async function hello(event, context) {
    try {
        console.log('[hello] hello called with event: ', event);

        const response = "Hello there - authenticated using JWT and authorized based on the User's scopes!";

        console.log('[hello] returned response: ', response);
        return apiResult.responseSuccess(response);

    } catch (error) {
        console.error('[hello] hello ' + error.stack)
        return apiResult.responseError(error);
    }
}

module.exports = {
    hello
}
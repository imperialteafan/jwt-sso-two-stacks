const userDDB = require('../database/userDDB');
const apiResult = require('../../../shared/utils/apiResult');

async function login (event, context) {
    try {
        console.log('[auth] #login - called with event.body: ', event.body);
        context.callbackWaitsForEmptyEventLoop = false;

        const parsedBody = JSON.parse(event.body);

        return userDDB.login(parsedBody.userId, parsedBody.password)
            .then(session => ({
                statusCode: 200,
                body: JSON.stringify(session)
            }))
            .catch(err => ({
                statusCode: err.statusCode || 500,
                headers: { 'Content-Type': 'text/plain' },
                body: JSON.stringify({ message: err.message })
            }));

    } catch (error) {
        console.error('[auth] login ' + error.stack)
        return apiResult.responseError(error);
    }

}

module.exports = {
    login
}
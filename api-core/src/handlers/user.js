const userDDB = require('../database/userDDB');
const apiResult = require('../../../shared/utils/apiResult');

async function postUser (event, context) {
    try {
        console.log('[user] #postUser called with event.body: ', event.body);
        context.callbackWaitsForEmptyEventLoop = false;

        const response = await userDDB.postUser(JSON.parse(event.body));

        console.log('[user] #postUser returned response: ', response);

        return apiResult.responseSuccess(response);

    } catch (error) {
        console.error('[user] #postUser ' + error.stack)
        return apiResult.responseError(error);
    }
}

async function getUsers (event, context) {
    try {
        console.log('[user] #getUsers called with event.body: ', event.body);
        context.callbackWaitsForEmptyEventLoop = false;

        const response = await userDDB.getUsers();

        console.log('[user] #getUsers returned response: ', response);

        return apiResult.responseSuccess(response);

    } catch (error) {
        console.error('[user] #getUsers ' + error.stack)
        return apiResult.responseError(error);
    }
}

module.exports = {
    postUser,
    getUsers
}
const bcrypt = require('bcryptjs-then');
const jwt = require('jsonwebtoken');

/**
 * Encrypts a password using bcrypt.
 * @param {*} password 
 * @param {*} saltLength 
 * @returns 
 */
function encryptPassword(password, saltLength = 10) {
    return bcrypt.hash(password, saltLength);
};

/**
 * Signs JWT token for user with given id.
 * @param {*} userId 
 * @returns 
 */
function signToken(userId, userScopes) {
    return jwt.sign({
            userId: userId,
            scopes: userScopes
        },
        process.env.JWT_SECRET,
        { expiresIn: 86400 * 2 } // 24 hours * 2
    );
}

/**
 * Compares hashed password provided in a request to the hashed password saved
 * in the DB.
 * @param {*} eventPassword 
 * @param {*} userPassword 
 * @returns 
 */
async function comparePasswords(eventPassword, userPassword, userId, userScopes) {

    return bcrypt.compare(eventPassword, userPassword)
        .then(isPasswordValid =>
            !isPasswordValid
                ? Promise.reject(new Error('Provided credentials are wrong.'))
                : signToken(userId, userScopes)
        );
};

module.exports = {
    encryptPassword,
    comparePasswords,
    signToken
}
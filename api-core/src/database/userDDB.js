const AWS = require('aws-sdk');

const { comparePasswords } = require('../utils/loginUtils');
const loginUtils = require('../utils/loginUtils');

let documentClient;

// offline
// const TABLE_NAME_USER = 'User';

// AWS.config.update({
//     region: 'eu-central-1',
//     endpoint: 'http://localhost:8000'
// });

// online
const TABLE_NAME_USER = process.env.TABLE_NAME_USER;

documentClient = new AWS.DynamoDB.DocumentClient();

/**
 * Returns token with User's data if the provided credentials are correct.
 * @param {*} userId - User's id
 * @param {*} password - User's password
 * @returns JWT token with User's data.
 */
async function login(userId, password) {
    try {
        console.log('[userDDB] login called with user: ', userId);

        const params = {
            TableName: TABLE_NAME_USER,
            Key: {
                userId
            }
        };

        const response = await documentClient
            .get(params)
            .promise()
            .then((data) =>
                !data.Item
                    ? Promise.reject(new Error(`Could not find user with id: ${userId}. `))
                    : comparePasswords(password, data.Item.password, data.Item.userId, data.Item.scopes)
            )
            .then(token => (
                {
                    auth: true,
                    token: token
                }
            ));

        return response;
    } catch (e) {
        // Throw InternalError
        let error = e;
        error.statusCode = 500;
        throw error;
    }
};

/**
 * Creates a new User, or updates existing one.
 * 
 * Simple endpoint used for creating new users in offline development.
 * @param {*} data 
 * @returns 
 */
async function postUser(data) {
    try {
        console.log('[userDDB] postUser called with data: ', data);
        let {
            userId,
            password
        } = data;

        if (!password) {
            throw new Error('No password was provided!');
        }

        // Encrypt the password.
        password = await loginUtils.encryptPassword(password);

        const params = {
            RequestItems: {
                [TABLE_NAME_USER]: [
                    {
                        PutRequest: {
                            Item: {
                                userId,
                                password
                            },
                        }
                    }
                ]
            }
        };

        let response = {
            userId: userId,
            password: password
        }

        await documentClient
            .batchWrite(params)
            .promise();

        return response;
    } catch (e) {
        // Throw InternalError
        let error = e;
        error.statusCode = 500;
        throw error;
    }
}

/**
 * Fetches list of all Users.
 * 
 * Simple endpoint used for fetching list of Users in offline development.
 * @returns 
 */
async function getUsers() {
    try {
        console.log('[userDDB] getUsers called');
        const params = {
            TableName: TABLE_NAME_USER
        };
        const response = await documentClient
            .scan(params, onScan)
            .promise();

        return response;
    } catch (e) {
        // Throw InternalError
        let error = e;
        error.message = 'occured while calling /getUsers endpoint'
        error.statusCode = 500;
        throw error;
    }
};

/**
 * Scans the whole table and returns the items.
 * @param {*} err 
 * @param {*} data 
 */
function onScan(err, data) {
    if (err) {
        console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
    } else {
        // continue scanning if we have more items, because
        // scan can retrieve a maximum of 1MB of data
        if (typeof data.LastEvaluatedKey != "undefined") {
            documentClient.scan(onScan);
        }
    }
}


module.exports = {
    login,
    postUser,
    getUsers
};